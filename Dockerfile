FROM debian:stable-slim
ARG userId=1000
RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8
RUN useradd -c "Node watcher user" -u ${userId} watcher
ADD target/node-watcher-linux-amd64 /node-watcher
USER watcher
CMD ["/node-watcher"]
