module gitlab.com/ds_2/k8s-node-watcher

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/ds_2/go-support-lib v1.1.8
	golang.org/x/sys v0.29.0
)
