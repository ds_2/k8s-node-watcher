# N8w8 Node Daemon

A dummy daemon to monitor a node.

## How to build

Run:

    export GO111MODULE=on
    export PKGDIR=$(pwd)/pkgs
    mkdir $PKGDIR
    go mod tidy
    go clean
    go get -pkgdir $PKGDIR
    go build -pkgdir $PKGDIR -race -o target/node-watcher
    GOOS=darwin GOARCH=amd64 go build -race -o target/node-watcher-macos
    GOOS=linux GOARCH=amd64 go build -ldflags "-extldflags '-static'" -o target/node-watcher-linux-amd64
    GOOS=windows GOARCH=amd64 go build -pkgdir pkg -race -ldflags "-extldflags '-static'" -o target/node-watcher-windows

or

    docker run -it --rm -v $(pwd):/work golang:latest go build -race -ldflags "-extldflags '-static'" -o target/node-watcher

## How to publish

Run

    docker build -f Dockerfile.full -t quay.io/ds2/n8w8-k8s-nodewatcher:latest .
    docker push quay.io/ds2/n8w8-k8s-nodewatcher:latest
