package main

import (
	"flag"
	"time"
)

var mountFolder = flag.String("mountFolder", "/", "Sets the folder that we query for checking")
var checkTimeout = flag.Duration("checkT0", 10*time.Second, "The timeout for checking the node again")
