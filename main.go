package main

import (
	"flag"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/ds_2/go-support-lib/sysinfo"
)

func main() {
	flag.Parse()
	logrus.Info("Using folder ", *mountFolder, " for testing.")
	for {
		diskSizeInfo, _ := sysinfo.GetDiskSizeInfo(*mountFolder)
		logrus.Debug("Disk Info: ", diskSizeInfo)
		time.Sleep(*checkTimeout)
	}
}
